# __init__.py
# Contact: Jacob Schreiber <jmschr@cs.washington.edu>
#          William Noble   <wnoble@uw.edu>

"""
Avocado is deep tensor factorization model for learning a latent representation
of the human epigenome. This package contains tools to implement the model and
use it yourself.
"""

__version__ = '0.1.0'